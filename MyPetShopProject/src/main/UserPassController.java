package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class UserPassController implements Initializable{
	
	@FXML private TextField utilizator;
	@FXML private PasswordField parola;
	@FXML private Button userPassButton;
	
	Socket socket;
	BufferedReader serverResponse;
	PrintWriter loginDataProvider;
	
	private static final String loginAccepted="Access granted";

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
            socket = new Socket("localhost", 5000);
            serverResponse = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            loginDataProvider =  new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            System.out.println("Nu s-a conectat la server");
        }
		
	}
	
	@FXML private void userPassButtonClicked() {
		
		if (parola.getText() != null && utilizator.getText() != null) {
			
			String username = utilizator.getText();
			String pass = parola.getText();
			
			loginDataProvider.println(username);
			loginDataProvider.println(pass);
			
			try {
				if(serverResponse.readLine().equals(loginAccepted)) {
					try {
						Scene currentScene=userPassButton.getScene();
						currentScene.getWindow().hide();
						
						FXMLLoader fxmlLoader = new FXMLLoader();
						fxmlLoader.setLocation(getClass().getResource("DoctorsView.fxml"));
						Scene scene = new Scene(fxmlLoader.load());
						Stage stage = new Stage();
						stage.setTitle("Pet Shop");
						stage.setScene(scene);
						stage.show();
				    
				    } catch (IOException e) {
				    	System.out.println("err");
				    }
				}
				else {
					Scene currentScene=userPassButton.getScene();
					currentScene.getWindow().hide();
					
					Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setHeaderText("Mai incearca!");
                    alert.setContentText("Nume utilizator sau parola gresita!");
                    alert.showAndWait();
                    
                    FXMLLoader fxmlLoader = new FXMLLoader();
					fxmlLoader.setLocation(getClass().getResource("UserPass.fxml"));
					Scene scene = new Scene(fxmlLoader.load());
					Stage stage = new Stage();
					stage.setTitle("Pet Shop");
					stage.setScene(scene);
					stage.show();
            	}
				
			    
			    } catch (IOException e) {
			    	System.out.println("Failed to create a new window.");
			    }
		}
		
	}
}
