package main;


import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.Programare;
import util.AnimalUtil;
import util.ProgramareUtil;

public class ViewController implements Initializable {
	private ProgramareUtil dbP = new ProgramareUtil();
	private AnimalUtil dbA = new AnimalUtil();

	@FXML private ListView<Programare> listProg = new ListView<>();
	@FXML private TextField specie;
	@FXML private TextField animal;
	@FXML private TextField stapan;
	@FXML private TextField varsta;
	@FXML private TextArea istoric;
	@FXML private DatePicker data;
	@FXML private ImageView poza;
	@FXML private Button inchideButton;
	@FXML private Button salveazaButton;
	
	int idAnimalModifica;
	
	@FXML private void salveazaButtonClicked() {
		String myObs;
		myObs=istoric.getText();
		dbA.updateIstoric(myObs, idAnimalModifica);
	}
	
	@FXML private void InchideButtonClicked() {
		Scene scenaCurenta=inchideButton.getScene();
		scenaCurenta.getWindow().hide();
	}
	
	@FXML private void pick() {
		lista.addAll(dbP.listAllAppointments(data));
		listProg.setItems(lista);
		
		listProg.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			idAnimalModifica=newValue.getAnimal().getIdAnimal();
			specie.setText(newValue.getAnimal().getSpecie());
			animal.setText(newValue.getAnimal().getNume());
			stapan.setText(newValue.getAnimal().getStapan());
			varsta.setText(newValue.getAnimal().getVarsta());
			data.setValue(dbA.listDates(newValue));
			poza.setImage(dbA.listImages(newValue));
			istoric.setText(newValue.getAnimal().getIstoric());
		});
		
		listProg.setCellFactory(list -> new ProgramareCell());
	}
	
	private ObservableList<Programare> lista = FXCollections.observableArrayList();
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		data.setValue(LocalDate.now());
		
		lista.addAll(dbP.listAllAppointments(data));
		listProg.setItems(lista);
		
		listProg.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			idAnimalModifica=newValue.getAnimal().getIdAnimal();
			specie.setText(newValue.getAnimal().getSpecie());
			animal.setText(newValue.getAnimal().getNume());
			stapan.setText(newValue.getAnimal().getStapan());
			varsta.setText(newValue.getAnimal().getVarsta());
			data.setValue(dbA.listDates(newValue));
			poza.setImage(dbA.listImages(newValue));
			istoric.setText(newValue.getAnimal().getIstoric());
		});
		
		listProg.setCellFactory(list -> new ProgramareCell());
}

static class ProgramareCell extends ListCell<Programare> {
	@Override
	protected void updateItem(Programare item, boolean bln) {
		super.updateItem(item, bln);
		if (item != null)
			setText("Programare " + item.getIdProgramare());
	    }
	}
}