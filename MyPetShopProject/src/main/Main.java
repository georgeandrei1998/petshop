package main;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import util.DatabaseGenerics;
import util.DatabaseUtil;

public class Main extends Application{

	public static void main(String[] args) throws Exception {
		DatabaseGenerics<DatabaseUtil> db =DatabaseGenerics.getInstance();
		launch(args);
		db.closeEntity();
	}
	
	public void start(Stage primaryStage) {
		try {
			FXMLLoader loader = new FXMLLoader(this.getClass().getResource("UserPass.fxml"));
			Parent parent = loader.load();
			Scene scene=new Scene (parent);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Pet Shop");
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}