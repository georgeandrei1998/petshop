package model;

import java.io.Serializable;
import javax.persistence.*;

import util.DatabaseGenerics;
import util.DatabaseUtil;

import java.util.List;


/**
 * The persistent class for the animal database table.
 * 
 */
@Entity
@NamedQuery(name="Animal.findAll", query="SELECT a FROM Animal a")
public class Animal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idAnimal;

	private byte[] imagine;

	private String istoric;

	private String nume;

	private String specie;

	private String stapan;

	private String varsta;

	//bi-directional many-to-one association to Programare
	@OneToMany(mappedBy="animal")
	private List<Programare> programares;

	public Animal() {
	}

	public int getIdAnimal() {
		return this.idAnimal;
	}

	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}

	public byte[] getImagine() {
		return this.imagine;
	}

	public void setImagine(byte[] imagine) {
		this.imagine = imagine;
	}

	public String getIstoric() {
		return this.istoric;
	}

	public void setIstoric(String istoric) {
		this.istoric = istoric;
	}

	public String getNume() {
		return this.nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getSpecie() {
		return this.specie;
	}

	public void setSpecie(String specie) {
		this.specie = specie;
	}

	public String getStapan() {
		return this.stapan;
	}

	public void setStapan(String stapan) {
		this.stapan = stapan;
	}

	public String getVarsta() {
		return this.varsta;
	}

	public void setVarsta(String varsta) {
		this.varsta = varsta;
	}

	public List<Programare> getProgramares() {
		return this.programares;
	}

	public void setProgramares(List<Programare> programares) {
		this.programares = programares;
	}

	public Programare addProgramare(Programare programare) {
		getProgramares().add(programare);
		programare.setAnimal(this);

		return programare;
	}

	public Programare removeProgramare(Programare programare) {
		getProgramares().remove(programare);
		programare.setAnimal(null);

		return programare;
	}
	
}