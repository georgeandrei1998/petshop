package util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import javafx.scene.control.DatePicker;
import model.Programare;

public class ProgramareUtil extends DatabaseUtil{
	
	/**
	 * search an appointment from DB
	 * @param programare
	 */
	public void retrieveProgramare(Programare programare) {
		programare = entityManager.find(Programare.class, programare.getIdProgramare());
	}
	
	/**
	 * update an appointment from DB
	 * @param programare
	 */
	public void updateProgramare(Programare programare) {
		Scanner scanner = new Scanner(System.in);
		String newName = (scanner.next());
		retrieveProgramare(programare);
		startTransaction();
		commitTransaction();
		scanner.close();
	}
	
	/**
	 * delete an appointment from DB
	 * @param programare
	 */
	public void deleteProgramare(Programare programare) {
		retrieveProgramare(programare);
		entityManager.getTransaction().begin();
		entityManager.remove(programare);
		entityManager.getTransaction().commit();
	}

	/**
	 * show in console all the appointments from DB
	 */
	public void printAllAppointmentsFromDB() {
		List<Programare> results = entityManager.createNativeQuery("Select * from mypetshop.Programare", Programare.class)
				.getResultList();
		System.out.println();
		for(Programare programare : results) {
			System.out.println("The appointment has ID: " 
					+ programare.getIdProgramare() + " and the date is: " 
					+ programare.getData());
		}
	}
	
	/**
	 *  show in console all the appointments and the foreign keys added to the table from DB
	 */
	public void printAllAppointmentsAndForeignKeys() {
		List<Programare> results = entityManager.createNativeQuery("Select * from mypetshop.Programare", Programare.class)
				.getResultList();
		System.out.println();
		for(Programare programare : results) {
			System.out.println("The appointment has ID: " 
					+ programare.getIdProgramare() + " and the date is: " 
					+ programare.getData() + " for the animal " + programare.getAnimal().getIdAnimal() 
					+ " at the medical personnel " + programare.getPersonalmedical().getIdPersonalMedical());
		}
	}
	
	public List<Programare> listAllAppointments(DatePicker date) {
		List<Programare> results = entityManager.createNativeQuery("Select * from mypetshop.Programare", Programare.class)
				.getResultList();
		Collections.sort(results, (p1, p2) -> p1.getData().compareTo(p2.getData()));
		List<Programare> currentDateProg = new ArrayList<>();
		for(Programare programare:results) {
			LocalDate currentDate=date.getValue();
			Instant instant = programare.getData().toInstant();
			LocalDate progDate = instant.atZone(ZoneId.systemDefault()).toLocalDate();
			if(progDate.equals(currentDate)==true) {
				currentDateProg.add(programare);
			}
		}
		return 	currentDateProg;
	}
	
	public List<Programare>listDiagnostic() {
		return entityManager.createNativeQuery("Select * from mypetshop.Animal, mypetshop.Programare where mypetshop.Animal.idAnimal = mypetshop.Programare.idAnimal", Programare.class)
				.getResultList();
	}
}
