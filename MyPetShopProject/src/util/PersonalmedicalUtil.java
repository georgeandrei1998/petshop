package util;

import java.util.List;
import java.util.Scanner;
import model.Personalmedical;

public class PersonalmedicalUtil extends DatabaseUtil {
	
	/**
	 * search medical personal in DB
	 * @param personalMedical
	 */
	public void retrievePersonalMedical(Personalmedical personalMedical) {
		personalMedical = entityManager.find(Personalmedical.class, personalMedical.getIdPersonalMedical());
	}
	
	/**
	 * update medical personal from DB
	 * @param personalMedical
	 */
	public void updatePersonalMedical(Personalmedical personalMedical) {
		Scanner scanner = new Scanner(System.in);
		String newName = (scanner.next());
		String newJob = (scanner.next());
		retrievePersonalMedical(personalMedical);
		startTransaction();
		personalMedical.setNume(newName);
		personalMedical.setFunctie(newJob);
		commitTransaction();
		scanner.close();
	}
	
	/**
	 * delete a medical personal from DB
	 * @param personalMedical
	 */
	public void deletePersonalMedical(Personalmedical personalMedical) {
		retrievePersonalMedical(personalMedical);
		entityManager.getTransaction().begin();
		entityManager.remove(personalMedical);
		entityManager.getTransaction().commit();
	}
	
	/**
	 * show all the medical personals from DB in console
	 */
	public void printAllMedicalPersonnelFromDB() {
		List<Personalmedical> results = entityManager.createNativeQuery("Select * from mypetshop.Personalmedical", Personalmedical.class)
				.getResultList();
		System.out.println();
		for(Personalmedical personalMedical : results) {
			System.out.println("The medical personnel: " + personalMedical.getNume() + 
					" has ID: " + personalMedical.getIdPersonalMedical() + 
					" and the job: "+ personalMedical.getFunctie());
		}
	}
}
