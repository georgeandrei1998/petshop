package util;

public class DatabaseGenerics<T extends DatabaseUtil> {
	
	private static DatabaseGenerics<DatabaseUtil> instance;
	
	static {
		try {
			instance = new DatabaseGenerics<>();
		}
		catch(Exception e) {
			System.out.println("Exception occured in creating singleton");
		}
	}
	
	public static DatabaseGenerics<DatabaseUtil> getInstance() {
		return instance;
	}
	
	private DatabaseGenerics() throws Exception {
		DatabaseUtil.setUp();
		DatabaseUtil.startTransaction();
		DatabaseUtil.commitTransaction();
	}

	/**
	 * save the dates in DB
	 * @param db
	 */
	public void addToDB(DatabaseGenerics<T> db) {
		DatabaseUtil.entityManager.persist(db);
	} 
	
	public void closeEntity() {
		DatabaseUtil.closeEntityManager();
	}

}
