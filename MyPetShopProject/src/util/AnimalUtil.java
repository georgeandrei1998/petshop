package util;

import java.io.ByteArrayInputStream;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Scanner;

import javafx.scene.image.Image;
import model.Animal;
import model.Programare;

public class AnimalUtil extends DatabaseUtil {
	
	/**
	 * search animal in DB
	 * @param animal
	 */
	public void retrieveAnimal(Animal animal) {
		animal = DatabaseUtil.entityManager.find(Animal.class, animal.getIdAnimal());
	}
	
	public int retrieveAnimalByIndex(int index) {
		Animal animal = DatabaseUtil.entityManager.find(Animal.class, index);
		return animal.getIdAnimal();
	}
	
	/**
	 * update animal from DB
	 * @param animal
	 */
	public void updateAnimal(Animal animal, String newName) {
		retrieveAnimal(animal);
		DatabaseUtil.startTransaction();
		animal.setNume(newName);
		DatabaseUtil.commitTransaction();
	}
	
	public void updateIstoric (String istoricNou, int id) {
		Animal animal = DatabaseUtil.entityManager.find(Animal.class, id);
		DatabaseUtil.startTransaction();
		animal.setIstoric(istoricNou);
		DatabaseUtil.commitTransaction();
	}
	
	/**
	 * add animal to DB
	 * @param animal
	 */
	public void addToDB(Animal animal) {
		DatabaseUtil.entityManager.persist(animal);
	} 
	
	/**
	 * delete animal from DB
	 * @param animal
	 */
	public void deleteAnimal(Animal animal) {
		retrieveAnimal(animal);
		DatabaseUtil.entityManager.getTransaction().begin();
		DatabaseUtil.entityManager.remove(animal);
		DatabaseUtil.entityManager.getTransaction().commit();
	}
	
	/**
	 * show all the animals from DB in console
	 */
	public void printAllAnimalsFromDB() {
		List<Animal> results = DatabaseUtil.entityManager.createNativeQuery("Select * from mypetshop.Animal", Animal.class)
				.getResultList();
		for(Animal animal : results) {
			System.out.println("Animal: " + animal.getNume() + " has ID: " + animal.getIdAnimal());
		}
	}
	
	public Image listImages(Programare prog) {
		Image img = null;
		List<Animal> results = entityManager.createNativeQuery("Select * from mypetshop.Animal, mypetshop.Programare where mypetshop.Animal.idAnimal = mypetshop.Programare.idAnimal", Animal.class)
				.getResultList();
		for(Animal animal: results) {
			if(prog.getAnimal().getIdAnimal() == animal.getIdAnimal())
				img = new Image(new ByteArrayInputStream(animal.getImagine()));
		}
		return img;
	}
	
	public LocalDate listDates(Programare prog) {
		LocalDate localDate = null;
		List<Animal> results = entityManager.createNativeQuery("Select * from mypetshop.Animal, mypetshop.Programare where mypetshop.Animal.idAnimal = mypetshop.Programare.idAnimal", Animal.class)
				.getResultList();
		for(Animal animal: results) {
			if(prog.getAnimal().getIdAnimal() == animal.getIdAnimal()) {
				Instant instant = prog.getData().toInstant();
				localDate = instant.atZone(ZoneId.systemDefault()).toLocalDate();
			}
		}
		return localDate;
	}
}
